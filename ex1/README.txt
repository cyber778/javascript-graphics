This project was created by Elad Silberring.


randomPixel() -  צור לולאה פשוטה שתשנה את המקדמים של X,Y
putpixel(x,y,color) - generates 1 px in the x,y point
drawLine() - algorithem bernsen for creating a line using putpixel()
drawPolygon() - function for drawing polygons using drawline()
drawCurve() - function using putpixel() to draw curves using calculations of points matrix
drawCircle() - function for drawing a circle on a given radius
console.log() this is used for me to keep track of what happens in the console

************MANUEL*************

this project is done in javascript and html5 technolegies,
To open project you need to click twice the index.html.

all drawing are done in canvas including the mouse clicks.

although this should work on all browsers it has only been checked in google chrome.

each button has a text inside explaining what it does,
after pressing the button you should follow instructions in top of page.

The buttons are:

- Change color - pick any of 8 colors
- Draw polygon - choose number of sides and 2 points
- Draw circle - choose 2 points
- Draw line - choose 2 points
- Draw curve - choose 4 points

arrayLoop() - calculation off the matrix given with a $.each loop

Move - (function moveIt()) moves object to desire place by calculations.
Scale - (function scaleIt()) usese matrix [sx,0,0,sy] where s is the scale size !! over 10 grows under 10 shrinks.
Shear - (shearIt()) - matrix-x[1,k,0,1] matrix-y [1,0,k,1].
Reflection/Mirror - (reflectIt()) uses default calculations to draw a mirror image.
