/*
 *  bersenhime line(2points) + circle(2 points) + curve(4 points) + polygon(2points)
 *  CREATED BY: Elad silberring
 *  Date: 15/03/14
 *
 */
var COLOR = "#000", CANVAS_WIDTH = 800, CANVAS_HEIGHT = 600, SIDES=0,
    c = document.getElementById("canvas"), ctx = c.getContext("2d"), ctx2 = c.getContext("2d"),
    clicks = 0, s_mode = 'line', x1, y1, x2, y2, x3, y3, x4, y4;

function putpixel(x, y, color) { //function for creating a single pixel given 1 point
    ctx.fillStyle = COLOR;
    ctx.fillRect(x, y, 1, 1);
}

function drawDashedLine(x1, y1, x2, y2) {//this is an extra for visualizing radius, not part of the ecersice
    ctx2.beginPath();
    ctx2.setLineDash([5]);
    ctx2.moveTo(x1, y1);
    ctx2.lineTo(x2, y2);
    ctx2.stroke();
}

function clearCanvas() {// clearing the canvas of pixels
    ctx.fillStyle = '#fff';
    ctx.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
}

function randomPixel() {//used for testing, not for H.W.
    var x, y;
    var i = 0;
    var color = '#3fede2';
    for ( i = 0; i < 1000; i++) {
        x = Math.floor((Math.random() * CANVAS_WIDTH));
        y = Math.floor((Math.random() * CANVAS_HEIGHT));
        color = String(x);
        putpixel(x, y, color);
    }
}

function drawLine(x1, y1, x2, y2) { // by using bernshime algo, we create a line with help of putpixel()
    var dx = (x2 - x1), dy = (y2 - y1), is_dx_positive = (dx > 0) ? true : false, is_dy_positive = (dy > 0) ? true : false, color = COLOR;

    dx = Math.abs(dx);
    //easier to caclculate when absolute
    dy = Math.abs(dy);
    putpixel(x1, y1, color);
    console.log(dx + " : " + dy, x1, y1, x2, y2);
    putpixel(x1, y1, color);
    if (dx > dy) {
        putpixel(x1, y1, color);
        err = 2 * dy - dx;
        //my "imaginary" line
        for ( i = dx; i > 0; i--) {
            x1 = (is_dx_positive) ? x1 + 1 : x1 - 1;
            //checkk in which direcrtion we need to draw the x
            if (err > 0) {
                y1 = (is_dy_positive) ? y1 + 1 : y1 - 1;
                err = err - (2 * dx);
            }
            err = err + (2 * dy);
            putpixel(x1, y1, color);
        }
    }
    else {
        err = 2 * dx - dy;
        //again....my "imaginary" line but with different params
        for ( i = dy; i > 0; i--) {
            y1 = (is_dy_positive) ? y1 + 1 : y1 - 1;
            //checkk in which direcrtion we need to draw the y
            if (err > 0) {
                x1 = (is_dx_positive) ? x1 + 1 : x1 - 1;
                err = err - (2 * dy);
            }
            err = err + (2 * dx);
            putpixel(x1, y1, color);
        }
    }
}

function drawCircle(x1, y1, x2, y2) { //circle by 2 points and caculating radius, also bernshime algo
    //x2 ,y2 is the center of the circle
    var dx = (x2 - x1), dy = (y2 - y1), 
        is_dx_positive = (dx > 0) ? true : false,
        is_dy_positive = (dy > 0) ? true : false,
    dx = Math.abs(dx);
    //easier to caclculate when absolute
    dy = Math.abs(dy);
    var radius = Math.sqrt((Math.pow(dy, 2) + Math.pow(dx, 2)));
    var dec = 3 - (2 * radius);
    //Calculating next decision parameter
    var x = 0, y = radius;
    while (x <= y)// for one eights, while x is not >= y  (45 Deg)
    {
        putpixel(x2 + x, y2 + y);
        putpixel(x2 - x, y2 + y);
        putpixel(x2 + x, y2 - y);
        putpixel(x2 - x, y2 - y);
        putpixel(x2 + y, y2 + x);
        putpixel(x2 - y, y2 + x);
        putpixel(x2 + y, y2 - x);
        putpixel(x2 - y, y2 - x);
        x++;
        if (dec < 0) {
            dec += 4 * x + 6;
        }
        else {
            y--;
            dec += 4 * (x - y) + 10;
        }
    }

    console.log('Rad: ' + radius);
    // print to console
    return;
}

function setTrackMouse(mode) { //used to get the point from the mouse click
    s_mode = mode;
    $('#canvas').off('click');
    trackMouse();
}
/*
 
taken from http://www.moshplant.com/direct-or/bezier/math.html
 
x(t) = axt3 + bxt2 + cxt + x0
y(t) = ayt3 + byt2 + cyt + y0



This method of definition can be reverse-engineered so that it'll give up the coefficient values based on the points described above:

cx = 3 (x1 - x0)
bx = 3 (x2 - x1) - cx
ax = x3 - x0 - cx - bx

cy = 3 (y1 - y0)
by = 3 (y2 - y1) - cy
ay = y3 - y0 - cy - by
 * */
function bezier(t, p0, p1, p2, p3){//used the above algo to create a curve by 4 points
    
    var cX = 3 * (p1.x - p0.x),
        bX = 3 * (p2.x - p1.x) - cX,
        aX = p3.x - p0.x - cX - bX;

    var cY = 3 * (p1.y - p0.y),
        bY = 3 * (p2.y - p1.y) - cY,
        aY = p3.y - p0.y - cY - bY;

    var x = (aX * Math.pow(t, 3)) + (bX * Math.pow(t, 2)) + (cX * t) + p0.x;
    var y = (aY * Math.pow(t, 3)) + (bY * Math.pow(t, 2)) + (cY * t) + p0.y;

    return {x : x, y : y};
}
   
function drawCurve(x1, y1, x2, y2, x3, y3, x4, y4){//a direct part of the above function for curve
    var p0 = {'x':x1,'y':y1},p1 = {'x':x2,'y':y2},p2 = {'x':x3,'y':y3},p3 = {'x':x4,'y':y4};
    var pix_amount = 1/ 1000;
    putpixel(p0.x, p0.y);
    for (var i=0; i<1; i+=pix_amount){ //where 0.001 is the amount of pixels that will be in the picture which will be x1000 pixels
     var p = bezier(i, p0, p1, p2, p3);
     putpixel(p.x, p.y);
  }
    
}     
 
function drawPolygon(x1, y1, x2, y2){ //draw polygon by calculaing start and end points for drawLine()
    var dx = (x2 - x1),
        dy = (y2 - y1), 
        is_dx_positive = (dx > 0) ? true : false,
        is_dy_positive = (dy > 0) ? true : false;
        
    dx = Math.abs(dx);
    dy = Math.abs(dy);
    
    var radius = Math.sqrt((Math.pow(dy, 2) + Math.pow(dx, 2)));
    var dec = 3 - (2 * radius);
    
    var old_x = y1,
        old_y = x1,
        new_y = y2,
        new_x = x2;
        
    var PI2 = Math.PI*2,
        sin = Math.sin,
        cos = Math.cos;
        
    var i,current,counter = parseInt(SIDES) + 1;  
    
    for (var i = 1; i<counter; i++) {
      current = PI2 * i / SIDES;
      old_y = new_y;
      old_x = new_x;
      new_x = (cos(current) * (x2-x1) - sin(current) * (y2-y1)) + x1; // calculating the next piece by angle to be drawn for x1
      new_y = (sin(current) * (x2-x1) + cos(current) * (y2-y1)) + y1; // calculating the next piece by angle to be drawn for y1


      drawLine(old_x, old_y, new_x, new_y );
      
    }
} 
 


function trackMouse() {//mouse tracking and UI functions, this decides what to draw, polygon, circle so on....
    $('h1').html('********Choose start point********');
    $('#canvas').on('click', function(e) {
        x = e.pageX - this.offsetLeft;
        y = e.pageY - this.offsetTop;
        console.log("clicks: " + clicks);
        console.log(s_mode);
        switch(s_mode) {
            case 'line':
                if (clicks == 0) {
                    x1 = x;
                    y1 = y;
                    clicks = 1;
                    $('h1').html('********Now the second point********');
                }
                else {
                    x2 = x;
                    y2 = y;
                    clicks = 0;
                    drawLine(x1, y1, x2, y2);
                    $('#output').append("line", ",", x1, "," , y1, ",", x2, ",", y2,"<br>");
                    $('h1').html('********Choose first point********');
                }
                break;
            case 'circle':
                if (clicks == 0) {
                    x1 = x;
                    y1 = y;
                    clicks = 1;
                    $('h1').html('********Now the second point********');
                }
                else {
                    x2 = x;
                    y2 = y;
                    clicks = 0;
                    console.log("clicks: " + clicks);
                    drawDashedLine(x1, y1, x2, y2);
                    //draws the radius line Dotted(not bernshime..)
                    drawCircle(x1, y1, x2, y2);
                    $('#output').append("circle", ",", x1, "," , y1, ",", x2, ",", y2,"<br>");
                    $('h1').html('********Choose first point********');
                }
                break;
            case 'curve':
                if (clicks == 0) {
                    x1 = x;
                    y1 = y;
                    clicks = 1;
                    $('h1').html('********Now the second point********');
                }
                else if (clicks == 1) {
                    x2 = x;
                    y2 = y;
                    clicks = 2;
                    drawDashedLine(x1, y1, x2, y2);
                    $('h1').html('********Now the third point********');
                }
                else if (clicks == 2) {
                    x3 = x;
                    y3 = y;
                    drawDashedLine(x2, y2, x3, y3);
                    clicks = 3;
                    $('h1').html('********Now the fourth point********');
                }
                else if (clicks == 3) {
                    x4 = x;
                    y4 = y;
                    drawDashedLine(x3, y3, x4, y4);
                    clicks = 0;
                    //draws the radius line Dotted for better tracking
                    drawCurve(x1, y1, x2, y2, x3, y3, x4, y4);
                    $('#output').append("curve", ",", x1, "," , y1, ",", x2, ",", y2, ",", x3, "," , y3, ",", x4, ",", y4,"<br>");
                    $('h1').html('********Choose first point********');
                }
                break;
            case 'polygon':
                if (clicks == 0) {
                    x1 = x;
                    y1 = y;
                    
                    clicks = 1;
                    $('h1').html('********Now the second point********');
                }
                else {
                    x2 = x;
                    y2 = y;
                    clicks = 0;
                    drawDashedLine(x1, y1, x2, y2);
                    //draws the radius line Dotted for better tracking
                    drawPolygon(x1, y1, x2, y2);
                    $('#output').append("polygon", ",",SIDES, ",", x1, ",", y1, ",", x2, ",", y2,"<br>");
                    $('h1').html('********Choose first point********');
                    break;
                }
                break;
            default:
                break;
            //code to be executed if n is different from case 1 and 2
        }

    });
}

//UI functions, these functions happen after page loads
$(function() {
    $('.color-icon').click(function() {
        COLOR = $(this).css('background-color');
        $('h1').html('****Choose whatyou want to do next****');
    });
    //color picker
    $('.color-btn').click(function() {
        $('h1').html('********Choose color********');
        $('#colors').slideToggle(700);
    });
    
    $('.polygon').click(function() {
        $('#canvas').hide();
        $('h1').html('********Choose number of sides********');
        $('#sides').slideToggle(700);
    });
    $('.sides-btn').click(function() {
        $('h1').html('You can now countinue,choose 1st point');
        SIDES = $('#sides input').val();
        if(SIDES<3){
            $('h1').html('*****Sorry number needs to be at least 3*****');
            return;
        }
        $('#sides').slideToggle(700,function(){
            
            $('#canvas').show();
        });
    });
    
});
