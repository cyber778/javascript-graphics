/*
 *  bersenhime line(2points) + circle(2 points) + curve(4 points) + polygon(2points)
 *  CREATED BY: Elad silberring
 *  Date: 15/03/14
 *
 */
var COLOR = "#000", CANVAS_WIDTH = 800, CANVAS_HEIGHT = 600, SIDES=0,
    c = document.getElementById("canvas"), ctx = c.getContext("2d"), ctx2 = c.getContext("2d"),
    x,y,deg,m_mode;
var cord_arr =[];

function putpixel(x, y, color) { //function for creating a single pixel given 1 point
    ctx.fillStyle = COLOR;
    ctx.fillRect(x, y, 1, 1);
}

function clearCanvas() {// clearing the canvas of pixels
    ctx.fillStyle = '#fff';
    ctx.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
}


function drawLine(x1, y1, x2, y2) { // by using bernshime algo, we create a line with help of putpixel()
    var dx = (x2 - x1), dy = (y2 - y1), is_dx_positive = (dx > 0) ? true : false, is_dy_positive = (dy > 0) ? true : false, color = COLOR;

    dx = Math.abs(dx);
    //easier to caclculate when absolute
    dy = Math.abs(dy);
    putpixel(x1, y1, color);
    putpixel(x1, y1, color);
    if (dx > dy) {
        putpixel(x1, y1, color);
        err = 2 * dy - dx;
        //my "imaginary" line
        for ( i = dx; i > 0; i--) {
            x1 = (is_dx_positive) ? x1 + 1 : x1 - 1;
            //checkk in which direcrtion we need to draw the x
            if (err > 0) {
                y1 = (is_dy_positive) ? y1 + 1 : y1 - 1;
                err = err - (2 * dx);
            }
            err = err + (2 * dy);
            putpixel(x1, y1, color);
        }
    }
    else {
        err = 2 * dx - dy;
        //again....my "imaginary" line but with different params
        for ( i = dy; i > 0; i--) {
            y1 = (is_dy_positive) ? y1 + 1 : y1 - 1;
            //checkk in which direcrtion we need to draw the y
            if (err > 0) {
                x1 = (is_dx_positive) ? x1 + 1 : x1 - 1;
                err = err - (2 * dy);
            }
            err = err + (2 * dx);
            putpixel(x1, y1, color);
        }
    }
}

function drawCircle(x1, y1, x2, y2) { //circle by 2 points and caculating radius, also bernshime algo
    //x2 ,y2 is the center of the circle
    var dx = (x2 - x1), dy = (y2 - y1), 
        is_dx_positive = (dx > 0) ? true : false,
        is_dy_positive = (dy > 0) ? true : false,
    dx = Math.abs(dx);
    //easier to caclculate when absolute
    dy = Math.abs(dy);
    var radius = Math.sqrt((Math.pow(dy, 2) + Math.pow(dx, 2)));
    var dec = 3 - (2 * radius);
    //Calculating next decision parameter
    var x = 0, y = radius;
    while (x <= y)// for one eights, while x is not >= y  (45 Deg)
    {
        putpixel(x2 + x, y2 + y);
        putpixel(x2 - x, y2 + y);
        putpixel(x2 + x, y2 - y);
        putpixel(x2 - x, y2 - y);
        putpixel(x2 + y, y2 + x);
        putpixel(x2 - y, y2 + x);
        putpixel(x2 + y, y2 - x);
        putpixel(x2 - y, y2 - x);
        x++;
        if (dec < 0) {
            dec += 4 * x + 6;
        }
        else {
            y--;
            dec += 4 * (x - y) + 10;
        }
    }

    return;
}

function setTrackMouse(mode) { //used to get the point from the mouse click
    s_mode = mode;
    $('#canvas').off('click');
    trackMouse();
}

function bezier(t, p0, p1, p2, p3){//used the above algo to create a curve by 4 points
    
    var cX = 3 * (p1.x - p0.x),
        bX = 3 * (p2.x - p1.x) - cX,
        aX = p3.x - p0.x - cX - bX;

    var cY = 3 * (p1.y - p0.y),
        bY = 3 * (p2.y - p1.y) - cY,
        aY = p3.y - p0.y - cY - bY;

    var x = (aX * Math.pow(t, 3)) + (bX * Math.pow(t, 2)) + (cX * t) + p0.x;
    var y = (aY * Math.pow(t, 3)) + (bY * Math.pow(t, 2)) + (cY * t) + p0.y;

    return {x : x, y : y};
}
   
function drawCurve(x1, y1, x2, y2, x3, y3, x4, y4){//a direct part of the above function for curve
    var p0 = {'x':x1,'y':y1},p1 = {'x':x2,'y':y2},p2 = {'x':x3,'y':y3},p3 = {'x':x4,'y':y4};
    var pix_amount = 1/ 1000;
    putpixel(p0.x, p0.y);
    for (var i=0; i<1; i+=pix_amount){ //where 0.001 is the amount of pixels that will be in the picture which will be x1000 pixels
     var p = bezier(i, p0, p1, p2, p3);
     putpixel(p.x, p.y);
  }
    
}     
 
function drawPolygon(x1, y1, x2, y2){ //draw polygon by calculaing start and end points for drawLine()
    var dx = (x2 - x1),
        dy = (y2 - y1), 
        is_dx_positive = (dx > 0) ? true : false,
        is_dy_positive = (dy > 0) ? true : false;
        
    dx = Math.abs(dx);
    dy = Math.abs(dy);
    
    var radius = Math.sqrt((Math.pow(dy, 2) + Math.pow(dx, 2)));
    var dec = 3 - (2 * radius);
    
    var old_x = y1,
        old_y = x1,
        new_y = y2,
        new_x = x2;
        
    var PI2 = Math.PI*2,
        sin = Math.sin,
        cos = Math.cos;
        
    var i,current,counter = parseInt(SIDES) + 1;  
    
    for (var i = 1; i<counter; i++) {
      current = PI2 * i / SIDES;
      old_y = new_y;
      old_x = new_x;
      new_x = (cos(current) * (x2-x1) - sin(current) * (y2-y1)) + x1; // calculating the next piece by angle to be drawn for x1
      new_y = (sin(current) * (x2-x1) + cos(current) * (y2-y1)) + y1; // calculating the next piece by angle to be drawn for y1


      drawLine(old_x, old_y, new_x, new_y );
    }
} 
 


function drawByCord(cord) {//mouse tracking and UI functions, this decides what to draw, polygon, circle so on....
    $('h1').html('********Choose numbers on axile********');
    var shape = cord[0];
    for(i=1;i<cord.length;i++)
        cord[i] = parseInt(cord[i]); //from string to number
    switch(shape) {
        case 'line':
            drawLine(cord[1], cord[2], cord[3], cord[4]); //translate to this -> (x1,y1,x2,y2)
            break;
        case 'circle':
            drawCircle(cord[1], cord[2], cord[3], cord[4]);
            break;
        case 'curve':
            drawCurve(cord[1], cord[2], cord[3], cord[4], cord[5], cord[6], cord[7], cord[8]);
            break;
        default:
            break;
        //code to be executed if n is different from case 1 and 2
    }
}

    function initCanvas(){
        cord_arr = [];
        $( "#input li" ).each(function( index ) {
          cord_arr.push($( this ).text());
          drawByCord(cord_arr[index].split(","));
        });
    }
    
    function changeCanvas(func_arr){
        $( "#input ul" ).html('');
        var arr=[],shape;
        $.each(func_arr,function( index, value) {
          arr = func_arr[index].split(',');
          shape = arr[0];
          if (shape != "curve")
            $( "#input ul" ).append("<li>"+arr[0] + ","+ arr[1] + "," + arr[2] + "," + arr[3] + "," + arr[4] + "</li>");
          else
            $( "#input ul" ).append("<li>"+arr[0] + "," + arr[1] + "," + arr[2] + ","+ arr[3] + ","+ arr[4] + "," + arr[5] + "," + arr[6] + "," + arr[7] + "," + arr[8] + "</li>");
        });
        initCanvas();
    }
    
    function moveIt(x,y){
        var x1,x2,x3,x4,y1,y2,y3,y4;
        var arr=[];
        var tmp_arr = [];
        $.each(cord_arr,function(index,value){
           arr = cord_arr[index].split(',');
           if(arr[0]=="curve"){
               x1 = parseInt(arr[1]) + x;
               y1 = parseInt(arr[2]) + y;
               x2 = parseInt(arr[3]) + x;
               y2 = parseInt(arr[4]) + y;
               x3 = parseInt(arr[5]) + x;
               y3 = parseInt(arr[6]) + y;
               x4 = parseInt(arr[7]) + x;
               y4 = parseInt(arr[8]) + y;
               tmp_arr[index] = arr[0] + "," + x1 + "," + y1 + "," + x2 + "," + y2 + "," + x3 + "," + y3 + "," + x4 + "," + y4;
           }
           else{
               x1 = parseInt(arr[1]) + x;
               y1 = parseInt(arr[2]) + y;
               x2 = parseInt(arr[3]) + x;
               y2 = parseInt(arr[4]) + y;
               tmp_arr[index] = arr[0] + "," + x1 + "," + y1 + "," +  x2 + "," + y2;
           }
        });
        clearCanvas();
        changeCanvas(tmp_arr);
        cord_arr = tmp_arr;
        
    }
    
    function calculateMatrix(m,m2){
        x1 = ((m[0] * m2[0])+(m[1]*m2[2]));
        x2 = ((m[0] * m2[1])+(m[1]*m2[3]));
        y1 = ((m[2] * m2[0])+(m[3]*m2[2]));
        y2 = ((m[2] * m2[1])+(m[3]*m2[3]));
        m=[x1,x2,y1,y2];
        return m; //line
    }
    
    function calculateMatrix2(m,m2,ms){
        x1 = ((m[0]*ms[0])+(m[1]*ms[2]));
        x2 = ((m2[0]*ms[0])+(m2[1]*ms[2]));
        y1 = ((m[0]*ms[1])+(m[1]*ms[3]));
        y2 = ((m2[0]*ms[1])+(m2[1]*ms[3]));
        m=[x1,x2,y1,y2];
        return m; //line
    }
    
    function arrayLoopbu(m2){
        var m =[];
        var x1,x2,x3,x4,y1,y2,y3,y4;
        var arr=[];
        var tmp_arr = [];
        $.each(cord_arr,function(index,value){
           arr = cord_arr[index].split(',');
           m=[parseInt(arr[1]),parseInt(arr[3]),parseInt(arr[2]),parseInt(arr[4])]; // m= matrix sm = scale matrix
           m=calculateMatrix(m,m2);
           x1 = m[0];
           x2 = m[1];
           y1 = m[2];
           y2 = m[3];
           if(arr[0]=="curve"){
               //m=[x1,x2,x3,x4,y1,y2,y3,y4];
               m=[parseInt(arr[5]),parseInt(arr[7]),parseInt(arr[6]),parseInt(arr[8])]; 
               m=calculateMatrix(m,m2);
               x3 = m[0];
               x4 = m[1];
               y3 = m[2];
               y4 = m[3];
               tmp_arr[index] = arr[0] + "," + x1 + "," + y1 + "," + x2 + "," + y2 + "," + x3 + "," + y3 + "," + x4 + "," + y4;
               
           }
           else{
               //matrix=[x1,x2,x3,x4...
               tmp_arr[index] = arr[0] + "," + x1 + "," + y1 + "," +  x2 + "," + y2;
           }
    });
    clearCanvas();
    changeCanvas(tmp_arr);
    cord_arr = tmp_arr;
    }
    
     function arrayLoop(ms){ //ms is the calc matrix(rotate matrix ect...)
        var m =[],m2=[];
        var x1,x2,x3,x4,y1,y2,y3,y4;
        var arr=[];
        var tmp_arr = [];
        $.each(cord_arr,function(index,value){
           arr = cord_arr[index].split(',');
           m=[parseInt(arr[1]),parseInt(arr[2])]; // m= matrix sm = scale matrix
           m2 = [parseInt(arr[3]),parseInt(arr[4])];
           m=calculateMatrix2(m,m2,ms);
           x1 = m[0];
           x2 = m[1];
           y1 = m[2];
           y2 = m[3];
           if(arr[0]=="curve"){
               //m=[x1,x2,x3,x4,y1,y2,y3,y4];
               m=[parseInt(arr[5]),parseInt(arr[6])]; 
               m2 = [parseInt(arr[7]),parseInt(arr[8])];
               m=calculateMatrix2(m,m2, ms);
               x3 = m[0];
               x4 = m[1];
               y3 = m[2];
               y4 = m[3];
               tmp_arr[index] = arr[0] + "," + x1 + "," + y1 + "," + x2 + "," + y2 + "," + x3 + "," + y3 + "," + x4 + "," + y4;
               
           }
           else{
               //matrix=[x1,x2,x3,x4...
               tmp_arr[index] = arr[0] + "," + x1 + "," + y1 + "," +  x2 + "," + y2;
           }
    });
    clearCanvas();
    changeCanvas(tmp_arr);
    cord_arr = tmp_arr;
    }
    
    function scaleIt(sx,sy){
        var sm=[sx,0,0,sy]; // sm = scale matrix
        arrayLoop(sm);
    }
    
    function shearIt(kx,ky){
        var m2 = [1,kx,0,1]; //x-axis
        arrayLoop(m2);
        var m2 = [1,0,ky,1]; //y-axis
        arrayLoop(m2);
    }
    
    function mirrorIt(){
        var arr = cord_arr[0].split(',');
        var m2=[-1,0,0,-1];
        arrayLoop(m2);
        moveIt(parseInt(arr[1])*2,parseInt(arr[2])*2); 
    }
    
    function mirrorItY(){
        var arr = cord_arr[0].split(',');
        var m2=[-1,0,0,1];
        arrayLoop(m2);
        moveIt(parseInt(arr[1])*2,0); 
    }
    
    function rotateIt(deg){
        var arr = cord_arr[0].split(',');
        var tmpx = parseInt(arr[1]), tmpy = parseInt(arr[2]);
        deg *= Math.PI / 180;
        var sm=[Math.cos(deg),Math.sin(deg),-Math.sin(deg),Math.cos(deg)]; // sm = scale matrix
        arrayLoop(sm);
    }
    
    function checkErrors(){
        if (m_mode!='rotate' && (!x && !y) ){
            $('.err').show();
            return;
        }
    }
    
    function manipulate(m_mode){
        $('.err').hide();
        x = parseInt($('input[name="x"]').val()) || 0;
        y = parseInt($('input[name="y"]').val())|| 0;
        deg = $('input[name="x"]').val() || 0;
        
        console.log(m_mode);
        switch(m_mode) {
        case 'move':
            moveIt(x,y); //translate to this -> (x1,y1,x2,y2)
            break;
        case 'derivative'://shear
            shearIt(x/10,y);
            break;
        case 'scale':
            scaleIt(x/10,x/10);
            break;
        case 'rotate':
            if(!deg){
                $('.err').show();
                return;
            }
            rotateIt(deg); //translate to this -> (x1,y1,x2,y2)
            break;
        case 'mirror-xy':
            mirrorIt();
            break;
        case 'mirror-yx':
            mirrorItY();
            break;
        default:
            break;
        //code to be executed if n is different from case 1 and 2
        }
    }

//UI functions, these functions happen after page loads
$(function() {
    $('#options .btn').click(function(){
        m_mode =  $(this).data('man');
        if($(this).hasClass('red')){
            $('#change .btn.green').click();
            return;
        }
        else if($(this).hasClass('orange')){
            $('.small-red.scale').show();
            $('.input-name').html('Scale:');
            $('.y-ax').hide();
        }
        else if($(this).hasClass('black')){
            $('.input-name').html('Deg:');
            $('.y-ax').hide();
        }
        else if($(this).hasClass('green')){
            $('.small-red.shear').show();
        }
        $('#options').hide();
        $('#change').show();
    });
    
    $('.back').click(function(){
        $('.input-name').html('X:');
        $('.y-ax').show();
        $('#change').hide();
        $('.small-red.scale').hide();
        $('.small-red.shear').hide();
        $('#options').show();
        $('.err').hide();
    });
    
    $('#change .btn.green').click(function(){
       manipulate(m_mode);
    });
    
    //handles the input file
    initCanvas();
    
});
